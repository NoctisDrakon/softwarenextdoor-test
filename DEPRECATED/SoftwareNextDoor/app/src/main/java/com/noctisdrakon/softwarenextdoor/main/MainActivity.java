package com.noctisdrakon.softwarenextdoor.main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import com.noctisdrakon.softwarenextdoor.R;
import com.noctisdrakon.softwarenextdoor.main.comments.CommentsActivity;

import java.util.List;

public class MainActivity extends AppCompatActivity implements Main.View, View.OnClickListener {

    private static final String TAG = "MainActivity";
    private RecyclerView postsRv;
    private MainPresenter presenter;
    private MainAdapter adapter;
    private RelativeLayout progressView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        postsRv = findViewById(R.id.posts_rv);
        progressView = findViewById(R.id.progress_view);
        postsRv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        initPresenter();
    }

    /**
     * Initializes the presenter and gets data from server
     */
    private void initPresenter() {
        presenter = new MainPresenter(this, getApplicationContext());
        presenter.getData();
    }

    @Override
    public void showProgress() {
        progressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressView.setVisibility(View.GONE);
    }

    @Override
    public void loadData(List<Post> list) {
        adapter = new MainAdapter(getApplicationContext(), list, this);
        postsRv.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        Post selectedPost = (Post) v.getTag();
        Intent newActivity = new Intent(getApplicationContext(), CommentsActivity.class);
        newActivity.putExtra("post", selectedPost);
        startActivity(newActivity);
    }
}
