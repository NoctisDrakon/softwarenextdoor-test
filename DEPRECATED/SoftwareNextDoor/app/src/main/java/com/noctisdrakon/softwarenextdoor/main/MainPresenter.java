package com.noctisdrakon.softwarenextdoor.main;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by NoctisDrakon on 6/8/2018.
 */

public class MainPresenter implements Main.Presenter {

    private static final String TAG = "MainPresenter";
    private Main.View view;
    private Context context;

    public MainPresenter(Main.View view, Context context) {
        this.view = view;
        this.context = context;
    }

    @Override
    public void getData() {
        view.showProgress();
        Ion.with(context)
                .load("https://jsonplaceholder.typicode.com/posts")
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {
                        List<Post> postsList;
                        Type listType = new TypeToken<List<Post>>() {
                        }.getType();
                        postsList = new Gson().fromJson(result, listType);
                        view.loadData(postsList);
                        view.hideProgress();
                    }
                });
    }
}
