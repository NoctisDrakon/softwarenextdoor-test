package com.noctisdrakon.softwarenextdoor.main;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.noctisdrakon.softwarenextdoor.R;

import java.util.List;

/**
 * Created by NoctisDrakon on 6/8/2018.
 */

public class MainAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "MainAdapter";
    private List<Post> items;
    private Context context;
    private View.OnClickListener listener;

    public MainAdapter(Context context, List<Post> items, View.OnClickListener listener) {
        this.context = context;
        this.items = items;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PostViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Post currentPost = getItem(position);
        ((PostViewHolder) holder).postContainer.setTag(currentPost);
        ((PostViewHolder) holder).postContainer.setOnClickListener(listener);
        ((PostViewHolder) holder).postTitle.setText(currentPost.title);
        ((PostViewHolder) holder).postBody.setText(currentPost.body);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    /**
     * Get a post from the items list
     *
     * @param position
     * @return the specified post
     */
    private Post getItem(int position) {
        return items.get(position);
    }

    public static class PostViewHolder extends RecyclerView.ViewHolder {
        LinearLayout postContainer;
        TextView postTitle;
        TextView postBody;

        PostViewHolder(View itemView) {
            super(itemView);
            postContainer = itemView.findViewById(R.id.post_container);
            postTitle = itemView.findViewById(R.id.post_title);
            postBody = itemView.findViewById(R.id.post_body);
        }
    }
}
