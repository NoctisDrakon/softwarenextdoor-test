package com.noctisdrakon.softwarenextdoor.main.comments;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import com.noctisdrakon.softwarenextdoor.R;
import com.noctisdrakon.softwarenextdoor.main.Post;

import java.util.List;

public class CommentsActivity extends AppCompatActivity implements Comments.View {

    private static final String TAG = "CommentsActivity";
    private RecyclerView commentsRv;
    private CommentsPresenter presenter;
    private CommentsAdapter adapter;
    private RelativeLayout progressView;
    private Post post;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        commentsRv = findViewById(R.id.comments_rv);
        progressView = findViewById(R.id.progress_view);
        commentsRv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        post = getIntent().getParcelableExtra("post");
        initPresenter();
    }

    @Override
    public void showProgress() {
        progressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressView.setVisibility(View.GONE);
    }

    @Override
    public void loadData(List<Comment> list) {
        adapter = new CommentsAdapter(this, list);
        commentsRv.setAdapter(adapter);
    }

    /**
     * Initializes the presenter
     */
    private void initPresenter() {
        presenter = new CommentsPresenter(this, getApplicationContext());
        presenter.getData(post.id);
    }
}
