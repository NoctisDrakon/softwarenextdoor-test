package com.noctisdrakon.softwarenextdoor.main.comments;

import com.noctisdrakon.softwarenextdoor.main.Post;

import java.util.List;

/**
 * Created by NoctisDrakon on 6/8/2018.
 */

public interface Comments {

    interface View {
        /**
         * Shows the progress view
         */
        void showProgress();

        /**
         * Hides the progress view
         */
        void hideProgress();

        /**
         * Loads received data into recyclerview
         *
         * @param list
         */
        void loadData(List<Comment> list);
    }

    interface Presenter {
        /**
         * Gets data from server
         */
        void getData(int postId);
    }
}
