package com.noctisdrakon.softwarenextdoor.main.comments;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.noctisdrakon.softwarenextdoor.main.Post;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by NoctisDrakon on 6/8/2018.
 */

public class CommentsPresenter implements Comments.Presenter {

    private static final String TAG = "CommentsPresenter";
    private Comments.View view;
    private Context context;

    public CommentsPresenter(Comments.View view, Context context) {
        this.view = view;
        this.context = context;
    }

    @Override
    public void getData(int postId) {
        view.showProgress();
        Ion.with(context)
                .load("https://jsonplaceholder.typicode.com/posts/" + postId + "/comments")
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {
                        List<Comment> postsList;
                        Type listType = new TypeToken<List<Comment>>() {
                        }.getType();
                        postsList = new Gson().fromJson(result, listType);
                        view.loadData(postsList);
                        view.hideProgress();
                    }
                });
    }
}
