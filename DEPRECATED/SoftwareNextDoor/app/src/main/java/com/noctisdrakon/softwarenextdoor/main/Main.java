package com.noctisdrakon.softwarenextdoor.main;

import java.util.List;

/**
 * Created by NoctisDrakon on 6/8/2018.
 */

public interface Main {

    interface View {
        /**
         * Shows the progress view
         */
        void showProgress();

        /**
         * Hides the progress view
         */
        void hideProgress();

        /**
         * Loads received data into recyclerview
         *
         * @param list
         */
        void loadData(List<Post> list);
    }

    interface Presenter {
        /**
         * Gets data from server
         */
        void getData();
    }

}
