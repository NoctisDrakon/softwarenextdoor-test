package com.noctisdrakon.softwarenextdoor.main;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NoctisDrakon on 6/8/2018.
 */

public class Post implements Parcelable {
    public int userId;
    public int id;
    public String title;
    public String body;


    protected Post(Parcel in) {
        userId = in.readInt();
        id = in.readInt();
        title = in.readString();
        body = in.readString();
    }

    public static final Creator<Post> CREATOR = new Creator<Post>() {
        @Override
        public Post createFromParcel(Parcel in) {
            return new Post(in);
        }

        @Override
        public Post[] newArray(int size) {
            return new Post[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(userId);
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(body);
    }
}
