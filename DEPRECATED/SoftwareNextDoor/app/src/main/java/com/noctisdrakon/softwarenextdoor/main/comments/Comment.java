package com.noctisdrakon.softwarenextdoor.main.comments;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NoctisDrakon on 6/8/2018.
 */

public class Comment implements Parcelable {

    public int postId;
    public int id;
    public String name;
    public String email;
    public String body;

    protected Comment(Parcel in) {
        postId = in.readInt();
        id = in.readInt();
        name = in.readString();
        email = in.readString();
        body = in.readString();
    }

    public static final Creator<Comment> CREATOR = new Creator<Comment>() {
        @Override
        public Comment createFromParcel(Parcel in) {
            return new Comment(in);
        }

        @Override
        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(postId);
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(body);
    }
}
