package com.noctisdrakon.softwarenextdoor.main.comments;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.noctisdrakon.softwarenextdoor.R;

import java.util.List;

/**
 * Created by NoctisDrakon on 6/8/2018.
 */

public class CommentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "MainAdapter";
    private List<Comment> items;
    private Context context;

    public CommentsAdapter(Context context, List<Comment> items) {
        this.context = context;
        this.items = items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CommentsAdapter.CommentViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Comment currentComment = getItem(position);
        ((CommentViewHolder) holder).commentTitle.setText(currentComment.name);
        ((CommentViewHolder) holder).commentBody.setText(currentComment.body);
        ((CommentViewHolder) holder).commentEmail.setText(currentComment.email);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    /**
     * Get a post from the items list
     *
     * @param position
     * @return the specified post
     */
    private Comment getItem(int position) {
        return items.get(position);
    }

    public static class CommentViewHolder extends RecyclerView.ViewHolder {
        LinearLayout commentContainer;
        TextView commentTitle;
        TextView commentBody;
        TextView commentEmail;

        CommentViewHolder(View itemView) {
            super(itemView);
            commentContainer = itemView.findViewById(R.id.comment_container);
            commentTitle = itemView.findViewById(R.id.comment_title);
            commentBody = itemView.findViewById(R.id.comment_body);
            commentEmail = itemView.findViewById(R.id.comment_email);
        }
    }
}
